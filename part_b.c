#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_NAME_LENGTH 51 // 50 characters + 1 for null terminator
#define MAX_REG_LENGTH 7   // 6 digits + 1 for null terminator
#define MAX_PROGRAM_LENGTH 5 // 4 characters + 1 for null terminator

// Structure to represent a student
struct Student {
    char name[MAX_NAME_LENGTH];
    char dob[11]; // YYYY-MM-DD + 1 for null terminator
    char regNumber[MAX_REG_LENGTH];
    char programCode[MAX_PROGRAM_LENGTH];
    float tuition;
    struct Student* next;
};

// Function prototypes
void displayMenu();
struct Student* createStudent();
void insertStudent(struct Student** head, struct Student* newStudent);
void displayStudents(struct Student* head);
void updateStudent(struct Student* head);
void deleteStudent(struct Student** head);

int main() {
    struct Student* head = NULL;
    int choice;

    do {
        displayMenu();
        printf("Enter your choice: ");
        scanf("%d", &choice);
        getchar(); // Consume newline character

        switch (choice) {
            case 1:
                insertStudent(&head, createStudent());
                break;
            case 2:
                displayStudents(head);
                break;
            case 3:
                updateStudent(head);
                break;
            case 4:
                deleteStudent(&head);
                break;
            case 5:
                printf("Exiting program.\n");
                break;
            default:
                printf("Invalid choice. Please try again.\n");
        }
    } while (choice != 5);

    return 0;
}

// Function to display menu options
void displayMenu() {
    printf("\nMenu:\n");
    printf("1. Add Student\n");
    printf("2. Display Students\n");
    printf("3. Update Student\n");
    printf("4. Delete Student\n");
    printf("5. Exit\n");
}

// Function to create a new student
struct Student* createStudent() {
    struct Student* newStudent = (struct Student*)malloc(sizeof(struct Student));
    if (newStudent == NULL) {
        printf("Memory allocation failed.\n");
        exit(1);
    }

    printf("Enter student name (max 50 characters): ");
    fgets(newStudent->name, MAX_NAME_LENGTH, stdin);
    newStudent->name[strcspn(newStudent->name, "\n")] = '\0'; // Remove newline character

    printf("Enter student date of birth (YYYY-MM-DD): ");
    fgets(newStudent->dob, 11, stdin);
    newStudent->dob[strcspn(newStudent->dob, "\n")] = '\0'; // Remove newline character

    printf("Enter student registration number (6 digits): ");
    fgets(newStudent->regNumber, MAX_REG_LENGTH, stdin);
    newStudent->regNumber[strcspn(newStudent->regNumber, "\n")] = '\0'; // Remove newline character

    printf("Enter student program code (max 4 characters): ");
    fgets(newStudent->programCode, MAX_PROGRAM_LENGTH, stdin);
    newStudent->programCode[strcspn(newStudent->programCode, "\n")] = '\0'; // Remove newline character

    printf("Enter student annual tuition: ");
    scanf("%f", &newStudent->tuition);
    getchar(); // Consume newline character

    newStudent->next = NULL;

    return newStudent;
}

// Function to insert a student into the linked list
void insertStudent(struct Student** head, struct Student* newStudent) {
    if (*head == NULL) {
        *head = newStudent;
    } else {
        struct Student* temp = *head;
        while (temp->next != NULL) {
            temp = temp->next;
        }
        temp->next = newStudent;
    }
    printf("Student added successfully.\n");
}

// Function to display all students in the linked list
void displayStudents(struct Student* head) {
    if (head == NULL) {
        printf("No students found.\n");
        return;
    }
    printf("\nList of Students:\n");
    while (head != NULL) {
        printf("Name: %s\n", head->name);
        printf("Date of Birth: %s\n", head->dob);
        printf("Registration Number: %s\n", head->regNumber);
        printf("Program Code: %s\n", head->programCode);
        printf("Annual Tuition: %.2f\n", head->tuition);
        printf("--------------------------\n");
        head = head->next;
    }
}

// Function to update a student's information
void updateStudent(struct Student* head) {
    if (head == NULL) {
        printf("No students found.\n");
        return;
    }

    char regNumber[MAX_REG_LENGTH];
    printf("Enter registration number of student to update: ");
    fgets(regNumber, MAX_REG_LENGTH, stdin);
    regNumber[strcspn(regNumber, "\n")] = '\0'; // Remove newline character

    while (head != NULL) {
        if (strcmp(head->regNumber, regNumber) == 0) {
            printf("Enter new student name (max 50 characters): ");
            fgets(head->name, MAX_NAME_LENGTH, stdin);
            head->name[strcspn(head->name, "\n")] = '\0'; // Remove newline character

            printf("Enter new student date of birth (YYYY-MM-DD): ");
            fgets(head->dob, 11, stdin);
            head->dob[strcspn(head->dob, "\n")] = '\0'; // Remove newline character

            printf("Enter new student program code (max 4 characters): ");
            fgets(head->programCode, MAX_PROGRAM_LENGTH, stdin);
            head->programCode[strcspn(head->programCode, "\n")] = '\0'; // Remove newline character

            printf("Enter new student annual tuition: ");
            scanf("%f", &head->tuition);
            getchar(); // Consume newline character

            printf("Student information updated successfully.\n");
            return;
        }
        head = head->next;
    }

    printf("Student with registration number %s not found.\n", regNumber);
}

// Function to delete a student from the linked list
void deleteStudent(struct Student** head) {
    if (*head == NULL) {
        printf("No students found.\n");
        return;
    }

    char regNumber[MAX_REG_LENGTH];
    printf("Enter registration number of student to delete: ");
    fgets(regNumber, MAX_REG_LENGTH, stdin);
    regNumber[strcspn(regNumber, "\n")] = '\0'; // Remove newline character

    struct Student* temp = *head;
    struct Student* prev = NULL;

    // If the student to be deleted is the head
    if (strcmp(temp->regNumber, regNumber) == 0) {
        *head = temp->next;
        free(temp);
        printf("Student deleted successfully.\n");
        return;
    }
    
        // Search for the student to delete
    while (temp != NULL && strcmp(temp->regNumber, regNumber) != 0) {
        prev = temp;
        temp = temp->next;
    }

    // If student not found
    if (temp == NULL) {
        printf("Student with registration number %s not found.\n", regNumber);
        return;
    }

    // Remove the student from the linked list
    prev->next = temp->next;
    free(temp);
    printf("Student deleted successfully.\n");
}


   
