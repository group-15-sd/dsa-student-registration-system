#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_NAME_LENGTH 51 // 50 characters + 1 for null terminator
#define MAX_REG_LENGTH 7   // 6 digits + 1 for null terminator
#define MAX_PROGRAM_LENGTH 5 // 4 characters + 1 for null terminator
#define FILE_NAME "student_records.csv"

// Structure to represent a student
struct Student {
    char name[MAX_NAME_LENGTH];
    char dob[11]; // YYYY-MM-DD + 1 for null terminator
    char regNumber[MAX_REG_LENGTH];
    char programCode[MAX_PROGRAM_LENGTH];
    float tuition;
    struct Student* next;
};

// Function prototypes
void displayMenu();
struct Student* createStudent();
void insertStudent(struct Student** head, struct Student* newStudent);
void displayStudents(struct Student* head);
void updateStudent(struct Student* head);
void deleteStudent(struct Student** head);
void searchStudentByRegNumber(struct Student* head);
void sortStudents(struct Student** head, int sortField);
void exportToCSV(struct Student* head);
void swap(struct Student* a, struct Student* b);

int main() {
    struct Student* head = NULL;
    int choice;

    do {
        displayMenu();
        printf("Enter your choice: ");
        scanf("%d", &choice);
        getchar(); // Consume newline character

        switch (choice) {
            case 1:
                insertStudent(&head, createStudent());
                break;
            case 2:
                displayStudents(head);
                break;
            case 3:
                updateStudent(head);
                break;
            case 4:
                deleteStudent(&head);
                break;
            case 5:
                searchStudentByRegNumber(head);
                break;
            case 6:
                sortStudents(&head, 0); // Sort by name by default
                break;
            case 7:
                sortStudents(&head, 1); // Sort by registration number
                break;
            case 8:
                exportToCSV(head);
                break;
            case 9:
                printf("Exiting program.\n");
                break;
            default:
                printf("Invalid choice. Please try again.\n");
        }
    } while (choice != 9);

    return 0;
}

// Function to display menu options
void displayMenu() {
    printf("\nMenu:\n");
    printf("1. Add Student\n");
    printf("2. Display Students\n");
    printf("3. Update Student\n");
    printf("4. Delete Student\n");
    printf("5. Search Student by Registration Number\n");
    printf("6. Sort Students by Name\n");
    printf("7. Sort Students by Registration Number\n");
    printf("8. Export Records to CSV\n");
    printf("9. Exit\n");
}

// Function to create a new student
struct Student* createStudent() {
    struct Student* newStudent = (struct Student*)malloc(sizeof(struct Student));
    if (newStudent == NULL) {
        printf("Memory allocation failed.\n");
        exit(1);
    }

    printf("Enter student name (max 50 characters): ");
    scanf("%49[^\n]", newStudent->name);
    getchar(); // Consume newline character

    printf("Enter student date of birth (YYYY-MM-DD): ");
    scanf("%10s", newStudent->dob);
    getchar(); // Consume newline character

    printf("Enter student registration number (6 digits): ");
    scanf("%5s", newStudent->regNumber);
    getchar(); // Consume newline character

    printf("Enter student program code (max 4 characters): ");
    scanf("%3s", newStudent->programCode);
    getchar(); // Consume newline character

    printf("Enter student annual tuition: ");
    scanf("%f", &newStudent->tuition);
    getchar(); // Consume newline character



    return newStudent;
}

// Function to insert a student into the linked list
void insertStudent(struct Student** head, struct Student* newStudent) {
    if (*head == NULL) {
        *head = newStudent;
    } else {
        struct Student* temp = *head;
        while (temp->next != NULL) {
            temp = temp->next;
        }
        temp->next = newStudent;
    }
    printf("Student added successfully.\n");
}

// Function to display all students in the linked list
void displayStudents(struct Student* head) {
    if (head == NULL) {
        printf("No students found.\n");
        return;
    }
    printf("\nList of Students:\n");
    while (head != NULL) {
        printf("Name: %s\n", head->name);
        printf("Date of Birth: %s\n", head->dob);
        printf("Registration Number: %s\n", head->regNumber);
        printf("Program Code: %s\n", head->programCode);
        printf("Annual Tuition: %.2f\n", head->tuition);
        printf("--------------------------\n");
        head = head->next;
    }
}

// Function to update a student's information
void updateStudent(struct Student* head) {
    if (head == NULL) {
        printf("No students found.\n");
        return;
    }

    char regNumber[MAX_REG_LENGTH];
    printf("Enter registration number of student to update: ");
    fgets(regNumber, MAX_REG_LENGTH, stdin);
    regNumber[strcspn(regNumber, "\n")] = '\0'; // Remove newline character

    int found = 0;

    while (head != NULL) {
        if (strcmp(head->regNumber, regNumber) == 0) {
            printf("Enter student name (max 50 characters): ");
            scanf("%49[^\n]", head->name);
            getchar(); // Consume newline character

            printf("Enter student date of birth (YYYY-MM-DD): ");
            scanf("%10s", head->dob);
            getchar(); // Consume newline character

            printf("Enter student program code (max 4 characters): ");
            scanf("%3s", head->programCode);
            getchar(); // Consume newline character

            printf("Enter student annual tuition: ");
            scanf("%f", &head->tuition);
            getchar(); // Consume newline character


            printf("Student information updated successfully.\n");
            found = 1; // Set flag to indicate student is found
            break; // Exit the loop if student is found and updated
        }
        head = head->next;
        if (!found){
                printf("Student with registration number %s not found.\n", regNumber);

        }
    }


}

// Function to delete a student from the linked list
void deleteStudent(struct Student** head) {
    if (*head == NULL) {
        printf("No students found.\n");
        return;
    }

    char regNumber[MAX_REG_LENGTH];
    printf("Enter registration number of student to delete: ");
    fgets(regNumber, MAX_REG_LENGTH, stdin);
    regNumber[strcspn(regNumber, "\n")] = '\0'; // Remove newline character

    struct Student* temp = *head;
    struct Student* prev = NULL;

    // If the student to be deleted is the head
    if (strcmp(temp->regNumber, regNumber) == 0) {
        *head = temp->next;
        free(temp);
        printf("Student deleted successfully.\n");
        return;
    }

    // Search for the student to delete
    while (temp != NULL && strcmp(temp->regNumber, regNumber) != 0) {
        prev = temp;
        temp = temp->next;
    }

    // If student not found
    if (temp == NULL) {
        printf("Student with registration number %s not found.\n", regNumber);
        return;
    }

    // Remove the student from the linked list
    prev->next = temp->next;
    free(temp);
    printf("Student deleted successfully.\n");
}

// Function to search for a student by their registration number
void searchStudentByRegNumber(struct Student* head) {
    if (head == NULL) {
        printf("No students found.\n");
        return;
    }

    char regNumber[MAX_REG_LENGTH];
    printf("Enter registration number of student to search: ");
    fgets(regNumber, MAX_REG_LENGTH, stdin);
    regNumber[strcspn(regNumber, "\n")] = '\0'; // Remove newline character

    while (head != NULL) {
        if (strcmp(head->regNumber, regNumber) == 0) {
            printf("Student found:\n");
            printf("Name: %s\n", head->name);
            printf("Date of Birth: %s\n", head->dob);
            printf("Registration Number: %s\n", head->regNumber);
            printf("Program Code: %s\n", head->programCode);
            printf("Annual Tuition: %.2f\n", head->tuition);
            return;
        }
        head = head->next;
    }

    printf("Student with registration number %s not found.\n", regNumber);
}

// Function to sort students based on the selected field
void sortStudents(struct Student** head, int sortField) {
    if (*head == NULL) {
        printf("No students found.\n");
        return;
    }

    struct Student *current, *nextNode;
    int sorted = 0;

    while (!sorted) {
        sorted = 1;
        current = *head;
        while (current->next != NULL) {
            nextNode = current->next;

            if (sortField == 0 && strcmp(current->name, nextNode->name) > 0) {
                swap(current, nextNode);
                sorted = 0;
            } else if (sortField == 1 && strcmp(current->regNumber, nextNode->regNumber) > 0) {
                swap(current, nextNode);
                sorted = 0;
            }

            current = current->next;
        }
    }

    // Print the sorted list based on the selected field
    printf("Sorted list:\n");
    while (*head != NULL) {
        if (sortField == 0)
            printf("%s, ", (*head)->name);
        else if (sortField == 1)
            printf("%s, ", (*head)->regNumber);

        *head = (*head)->next;
    }
    printf("\n");
}

// Function to swap two student nodes
void swap(struct Student* a, struct Student* b) {
    struct Student temp = *a;
    *a = *b;
    *b = temp;
}

// Function to export records to CSV file
void exportToCSV(struct Student* head) {
    FILE* fp = fopen(FILE_NAME, "a"); // Open file in append mode
    if (fp == NULL) {
        printf("Error opening file for writing.\n");
        return;
    }

    while (head != NULL) {
        fprintf(fp, "%s,%s,%s,%s,%.2f\n", head->name, head->dob, head->regNumber, head->programCode, head->tuition);
        head = head->next;
    }

    fclose(fp);
    printf("Records exported to %s successfully.\n", FILE_NAME);
}