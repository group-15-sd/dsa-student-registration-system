# DSA STUDENT REGISTRATION SYSTEM

<!-- below are the links to individual youtube videos explaining the individual code-->
NAME                        LINK                                                    REG NUMBER

KABOGERE SALIMU:            https://youtu.be/jZNc2AsZZdA?si=BYnqjJYDT4Y82jCW        2300700459

KATENDE RONNIE:             https://youtu.be/tsfb9n_cokU?si=5Zz2tldGLCD8OcBe        23/U/09239/PS

KASHARA ALVIN SSALI:        https://youtu.be/0VeEflzX5DM?si=CRQ6cB3cdCT9CNbl        23/U/09144/PS

NASSALI CATHERINE LAURA:   https://youtu.be/spYZHGgEYfs?si=DUCMrzMxd0tvaIuC        23/U/15945/PS

MATHIAS LUGANZI JOSEPH:    https://youtu.be/Ve5cd8NCSI8?si=cR48ST4R6TBKyjAr         23/U/24122/PS

# This README describes the considerations that were taken to choose linked lists as the best option for our project

1. Dynamic Size:

   The student management system at SCIT needs to handle a variable number of students records.
   Linked lists offer dynamic memory locations, allowing the structure to grow or shrink without wasting memory
   this means that the system can accomodate any number of students without pre-allocating a fixed memory size

2. Efficient Insertion and Deletion:

   With students enrolling and graduating, the system must efficiently handle insertion and deletion of student records.
   Linked lists provide efficient insertion and deletion operations, especially when adding or removing students from the middle of the list.
   This allows the system to quickly update and maintain the student database

3. Memory Efficiency:

   Each student record in the system contains specific details such as name, date of birth, registration number, program code, and annual tuition.
   Linked lists offer memory efficiency by storing only the necessary data for each student and a pointer to the next student, minimizing memory overhead.

4. Flexibility in Sorting and Searching:

   While linked lists may not be the most efficient data structure for searching and sorting, they still offer reasonable performance.
   The system can implement sorting and searching algorithms on the linked list to efficiently retrieve and organize student records based on various criteria, such as name or registration number.

5. Ease of Implementation:

   Linked lists are relatively simple to implement and understand.
   This makes them suitable for projects where simplicity and ease of maintenance are important factors.
   The system developers at SCIT can easily implement and manage the linked list structure for storing and managing student records.
