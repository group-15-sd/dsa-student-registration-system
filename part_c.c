#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_NAME_LENGTH 51 // 50 characters + 1 for null terminator
#define MAX_REG_LENGTH 7   // 6 digits + 1 for null terminator
#define MAX_PROGRAM_LENGTH 5 // 4 characters + 1 for null terminator
struct Student* createStudent() {
    struct Student* newStudent = (struct Student*)malloc(sizeof(struct Student));
    if (newStudent == NULL) {
        printf("Memory allocation failed.\n");
        exit(1);
    }

    printf("Enter student name (max 50 characters): ");
    fgets(newStudent->name, MAX_NAME_LENGTH, stdin);
    newStudent->name[strcspn(newStudent->name, "\n")] = '\0'; // Remove newline character

    printf("Enter student date of birth (YYYY-MM-DD): ");
    fgets(newStudent->dob, 11, stdin);
    newStudent->dob[strcspn(newStudent->dob, "\n")] = '\0'; // Remove newline character

    printf("Enter student registration number (6 digits): ");
    fgets(newStudent->regNumber, MAX_REG_LENGTH, stdin);
    newStudent->regNumber[strcspn(newStudent->regNumber, "\n")] = '\0'; // Remove newline character

    printf("Enter student program code (max 4 characters): ");
    fgets(newStudent->programCode, MAX_PROGRAM_LENGTH, stdin);
    newStudent->programCode[strcspn(newStudent->programCode, "\n")] = '\0'; // Remove newline character

    printf("Enter student annual tuition: ");
    scanf("%f", &newStudent->tuition);
    getchar(); // Consume newline character

    newStudent->next = NULL;

    return newStudent;
}

// Function to insert a student into the linked list
void insertStudent(struct Student** head, struct Student* newStudent) {
    if (*head == NULL) {
        *head = newStudent;
    } else {
        struct Student* temp = *head;
        while (temp->next != NULL) {
            temp = temp->next;
        }
        temp->next = newStudent;
    }
    printf("Student added successfully.\n");
}

// Function to display all students in the linked list
void displayStudents(struct Student* head) {
    if (head == NULL) {
        printf("No students found.\n");
        return;
    }
    printf("\nList of Students:\n");
    while (head != NULL) {
        printf("Name: %s\n", head->name);
        printf("Date of Birth: %s\n", head->dob);
        printf("Registration Number: %s\n", head->regNumber);
        printf("Program Code: %s\n", head->programCode);
        printf("Annual Tuition: %.2f\n", head->tuition);
        printf("--------------------------\n");
        head = head->next;
    }
}

// Function to update a student's information
void updateStudent(struct Student* head) {
    if (head == NULL) {
        printf("No students found.\n");
        return;
    }

    char regNumber[MAX_REG_LENGTH];
    printf("Enter registration number of student to update: ");
    fgets(regNumber, MAX_REG_LENGTH, stdin);
    regNumber[strcspn(regNumber, "\n")] = '\0'; // Remove newline character

    while (head != NULL) {
        if (strcmp(head->regNumber, regNumber) == 0) {
            printf("Enter new student name (max 50 characters): ");
            fgets(head->name, MAX_NAME_LENGTH, stdin);
            head->name[strcspn(head->name, "\n")] = '\0'; // Remove newline character

            printf("Enter new student date of birth (YYYY-MM-DD): ");
            fgets(head->dob, 11, stdin);
            head->dob[strcspn(head->dob, "\n")] = '\0'; // Remove newline character

            printf("Enter new student program code (max 4 characters): ");
            fgets(head->programCode, MAX_PROGRAM_LENGTH, stdin);
            head->programCode[strcspn(head->programCode, "\n")] = '\0'; // Remove newline character

            printf("Enter new student annual tuition: ");
            scanf("%f", &head->tuition);
            getchar(); // Consume newline character

            printf("Student information updated successfully.\n");
            return;
        }
        head = head->next;
    }

    printf("Student with registration number %s not found.\n", regNumber);
}

// Function to delete a student from the linked list
void deleteStudent(struct Student** head) {
    if (*head == NULL) {
        printf("No students found.\n");
        return;
    }

    char regNumber[MAX_REG_LENGTH];
    printf("Enter registration number of student to delete: ");
    fgets(regNumber, MAX_REG_LENGTH, stdin);
    regNumber[strcspn(regNumber, "\n")] = '\0'; // Remove newline character

    struct Student* temp = *head;
    struct Student* prev = NULL;

    // If the student to be deleted is the head
    if (strcmp(temp->regNumber, regNumber) == 0) {
        *head = temp->next;
        free(temp);
        printf("Student deleted successfully.\n");
        return;
    }

    // Search for the student to delete
    while (temp != NULL && strcmp(temp->regNumber, regNumber) != 0) {
        prev = temp;
        temp = temp->next;
    }

    // If student not found
    if (temp == NULL) {
        printf("Student with registration number %s not found.\n", regNumber);
        return;
    }

    // Remove the student from the linked list
    prev->next = temp->next;
    free(temp);
    printf("Student deleted successfully.\n");
}

// Function to search for a student by their registration number
void searchStudentByRegNumber(struct Student* head) {
    if (head == NULL) {
        printf("No students found.\n");
        return;
    }

    char regNumber[MAX_REG_LENGTH];
    printf("Enter registration number of student to search: ");
    fgets(regNumber, MAX_REG_LENGTH, stdin);
    regNumber[strcspn(regNumber, "\n")] = '\0'; // Remove newline character

    while (head != NULL) {
        if (strcmp(head->regNumber, regNumber) == 0) {
            printf("Student found:\n");
            printf("Name: %s\n", head->name);
            printf("Date of Birth: %s\n", head->dob);
            printf("Registration Number: %s\n", head->regNumber);
            printf("Program Code: %s\n", head->programCode);
            printf("Annual Tuition: %.2f\n", head->tuition);
            return;
        }
        head = head->next;
    }

    printf("Student with registration number %s not found.\n", regNumber);
}
