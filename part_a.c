#include <stdio.h>
#include <stdlib.h>

#define MAX_NAME_LENGTH 51 // 50 characters + 1 for null terminator
#define MAX_REG_LENGTH 7   // 6 digits + 1 for null terminator
#define MAX_PROGRAM_LENGTH 5 // 4 characters + 1 for null terminator

// Structure to represent a student
struct Student {
    char name[MAX_NAME_LENGTH];
    char dob[11]; // YYYY-MM-DD + 1 for null terminator
    char regNumber[MAX_REG_LENGTH];
    char programCode[MAX_PROGRAM_LENGTH];
    float tuition;
};

int main() {
    struct Student student1;

    // Prompting user for input
    printf("Enter student name (max 50 characters):\n");
    scanf("%50s", student1.name);
    
    printf("Enter student date of birth (YYYY-MM-DD):\n");
    scanf("%10s", student1.dob);
    
    printf("Enter student registration number (6 digits):\n ");
    scanf("%6s", student1.regNumber);
    
    printf("Enter student program code (max 4 characters):\n ");
    scanf("%4s", student1.programCode);
    
   printf("Enter student annual tuition: \n");
    if (scanf("%f", &student1.tuition) != 1 || student1.tuition < 0) {
        printf("Invalid input for tuition.\n");
        exit(EXIT_FAILURE);
    }


    // Printing student information
    printf("\nStudent Information:\n");
    printf("Name: %s\n", student1.name);
    printf("Date of Birth: %s\n", student1.dob);
    printf("Registration Number: %s\n", student1.regNumber);
    printf("Program Code: %s\n", student1.programCode);
    printf("Annual Tuition: %.2f\n", student1.tuition);

    return 0;
}
